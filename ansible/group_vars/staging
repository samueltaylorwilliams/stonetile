---
server_name: staging
domain: magento.wcltest.com
backup_ip: 10.3.10.222 # 10.3.10.221 for production, 10.3.10.222 for staging

####################
## Magento config ##
####################
mage_mode: "production"

nginx_ssl_enabled: true
nginx_vhosts:
  - listen: "80"
    server_name: "{{ domain }}"
#    extra_parameters: |
#      return 301 https://{{ domain }}; # Only needed when not using varnish
  - listen: "443 ssl"
    server_name: "{{ domain }}"
    index: "index.php index.html index.htm"
    extra_parameters: |
      ssl_prefer_server_ciphers on;
      ssl_session_cache shared:SSL:10m;
      ssl_protocols TLSv1.2;
      ssl_ciphers 'ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS';
      ssl_certificate {{ ssl_certificate_remote_file }};
      ssl_certificate_key {{ ssl_certificate_key_remote_file }};

###########
## Mysql ##
###########
mysql_root_password: "CHANGEME"
mysql_databases:
  - name: _staging
    encoding: utf8
    collation: utf8_general_ci
mysql_users:
  - name: _staging
    host: "%"
    password: CHANGEME
    priv: "_staging.*:ALL"

##################
## SMTP details ##
##################
custom_smtp_enabled: false
smtp_user: someuser
smtp_password: somepassword
email_domain: example.com
smtp_send_from: hello@example.com
smtp_host: smtp.example.com
smtp_port: 587
redirect_all_mail: false
redirect_to: emailredirect@example.com
#mail_domain_restrict: true
#mail_domain_restrict_list:
#  - customerdomain.com
### Williams commerce domain is allowed by default

###########
## Redis ##
###########
redis_bind_interface: 127.0.0.1
redis_maxmemory: 512mb
redis_maxmemory_policy: allkeys-lru
### Options for redis_maxmemory_policy are:
# volatile-lru -> Evict using approximated LRU among the keys with an expire set.
# allkeys-lru -> Evict any key using approximated LRU.
# volatile-lfu -> Evict using approximated LFU among the keys with an expire set.
# allkeys-lfu -> Evict any key using approximated LFU.
# volatile-ttl -> Remove the key with the nearest expire time (minor TTL)
# noeviction -> Don't evict anything, just return an error on write operations.

#############
## Varnish ##
#############
varnish_enabled: true

###################
## Elasticsearch ##
###################
elasticsearch_enabled: true
elasticsearch_memory: 512M
elasticsearch_port: 9200
elasticsearch_prefix: magento2
elasticsearch_host: localhost

############
## Zabbix ##
############
zabbix_enabled: true
zabbix_env: staging

##############
## Yum Cron ##
##############
yum_cron_enabled: true
random_reboot_enabled: true

######################################
## List of virtual users for vsftpd ##
######################################
vsftpd_enabled: false
vsftpd_users:
  - username: exampleuser
    password: examplepassword
    user_map_as: "{{ deploy_user }}"
    user_home_dir: "{{ document_root }}"
    chroot_enable: "yes"
    local_umask: "003"
    file_open_mode: "0664"

##########################################################################################
## htpasswd                                                                             ##
## You can generate user/password here http://www.htaccesstools.com/htpasswd-generator/ ##
## The password should be the hashed version e.g. $apr1$z14.Mn/S$zYOPsAem1RJsRGY4UyapJ/ ##
##########################################################################################
nginx_basic_auth_enabled: true
htpasswd_users:
  - username: wlctstsys
    password: $apr1$8aKe8DT7$wO3S.5hYhP/7A9fQ37KFh1
#basic_auth_ip_allowed_list:
#  - "IP goes here"

#####################
## Nginx Redirects ##
#####################
nginx_rewrites_enabled: false
nginx_rewrites_file: nginxrewrites

###############################################################
## Force robots.txt file to disallow search engines crawling ##
###############################################################
force_no_index_no_follow: true

backups_enabled: false
backups_mount_destination: '{{ backup_ip }}:/volume1/{{ domain }}'
